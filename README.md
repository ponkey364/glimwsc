# GlimWSC: Glimesh Websockets for Go

A library that removes the pain from handling the phoenix websockets that Glimesh uses in Go.

## Usage

Install with go get: `go get gitlab.com/ponkey364/glimwsc`

## Subscribing to Events

## Sending Queries and Mutations

This library automatically handles getting a response and handing back to you.

Let's say we have a username, and we want to find their ID.

With an open session, call `session.DoQuery` with the query as the first parameter, we'll use

```graphql
query ($username: String) {
    user (username: $username) {
        id
    }
}
```

The second parameter to DoQuery should be the variables, in the form of a `map[string]interface{}`. For this query,
we'll use `map[string]interface{}{"username": "ponkey364"}`.

GlimWSC will return a `json.RawMessage` (which is an alias of `[]byte`). If we fmt.Println this, we'll get this json:

```json
{
  "response": {
    "data": {
      "user": {
        "id": "4931"
      }
    }
  },
  "status": "ok"
}
```

We can then parse the JSON as we usually would, as if we'd make a request using a traditional POST.

```go
type data struct {
    Response struct {
        Data struct {
            User struct {
                ID string
            }	
        }
    }
}
```

Despite the name of this function, all of this also applies to Mutations, and will work identically.