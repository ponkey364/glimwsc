package glimwsc

import "encoding/json"

type startSubscriptionResponse struct {
	Response struct {
		SubscriptionId string
	}
	Status string
}

type OnSubscription struct {
	Result struct {
		Data json.RawMessage
	}
}

func (s *Session) StartSubscription(query string, variables map[string]interface{}, handler func(*Session, json.RawMessage)) error {
	data := s.DoQuery(query, variables)

	resp := &startSubscriptionResponse{}

	err := json.Unmarshal(data, resp)
	if err != nil {
		return err
	}

	s.subscriptions[resp.Response.SubscriptionId] = handler

	return nil
}
